/*===== Global variable =====*/
let deck = [];
let deckBack = [];
const types = ['C', 'D', 'H', 'S'];
const specials = ['A', 'J', 'Q', 'K'];

let imgCard = '';

let pointPlayer = 0,
    pointComputer = 0;

/*===== Buttons variable =====*/
const btnNew = document.querySelector('#btnNew');
const btnTakeCard = document.querySelector('#btnTakeCard');
const btnStop = document.querySelector('#btnStop');

/*===== HTML variable =====*/
const scores = document.querySelectorAll('small');
const tagImg = document.querySelectorAll('img');


const divCardPlayer = document.querySelector('#divCardPlayer');
const divCardComputer = document.querySelector('#divCardComputer');
const divCardBack = document.querySelector('#divCardBack');

btnStop.disabled = true;
btnTakeCard.disabled = true;

/*==================== CREATE DECK ====================*/
const createDeck = () => {

    /* Insert the number more types in the deck */
    for (let i = 2; i <= 10; i++) {
        // console.log(i);
        for (type of types) {
            deck.push(i + type)
        }
    }

    /* Insert the number more specials in the deck */
    for (spe of specials) {
        // console.log(spe);
        for (type of types) {
            deck.push(spe + type);
        }
    }

    /* .-shuffle: returns the randomly shuffed deck of cards    */
    deck = _.shuffle(deck);
    // console.log(deck);
}

/*==================== TAKE DECK ====================*/
const takeCard = () => {

    if (deck.length === 0) {
        console.log('Don´t card in the deck');
    }
    let card = deck.pop();
    return card;
}

/*==================== VALUE CARD ====================*/
const valueCard = (card) => {
    let value = card.substring(0, card.length - 1);
    value = (isNaN(value)) ? (value === 'A') ? 11 : 10 : value * 1;
    return value;
}

/*==================== CREATE ELEMENT IMG CARD ====================*/
const createImgCArd = (card) => {
    let imgCard = document.createElement('img');
    imgCard.classList.add('img');
    imgCard.src = `assets/cards/${card}.png`;

    setTimeout(() => {
        imgCard.classList.remove('img');
        imgCard.classList.add('cardM');
    }, 100);

    let cardB = deckBack.pop();
    cardB.classList.add('cardMov');

    setTimeout(() => {
        cardB.remove();
    }, 500);

    return imgCard;
}

/*==================== CREATE ELEMENT IMG BACK CARD ====================*/
const createBackCard = (deck) => {

    for (let i = 1; i <= deck.length; i++) {

        imgCard = document.createElement('img');
        imgCard.classList.add('cardBack');
        imgCard.id = `card${i}`
        imgCard.src = `assets/cards/grey_back.png`;

        deckBack.push(imgCard);
        divCardBack.append(imgCard);

    }
}

/*==================== COMPUTER SHITF ====================*/

const computerShitf = (pointMin) => {

    do {
        let card = takeCard();
        pointComputer = pointComputer + valueCard(card);

        let imgCard = document.createElement('img');
        imgCard.classList.add('cardM');
        imgCard.src = `assets/cards/${card}.png`;
        divCardComputer.append(imgCard);
        scores[1].innerText = `Score: ${pointComputer}`;

        if (pointMin > 21) {
            break;
        }

    } while ((pointPlayer > pointComputer) && (pointMin <= 21))

    setTimeout(() => {
        if (pointComputer === pointMin) {
            alert('Nobody Win!');
        } else if (pointMin > 21) {
            alert('You lost, the computer won!');

        } else if (pointComputer > 21) {
            alert('You won, the computer lost!');

        } else {
            alert('You lost, the computer won!');
        }
    }, 1000);
}

/*==================== Envents ====================*/

btnTakeCard.addEventListener('click', () => {
    let card = takeCard();
    console.log('Card:', card);

    pointPlayer = pointPlayer + valueCard(card);

    let imgCard = createImgCArd(card);


    setTimeout(() => {
        divCardPlayer.append(imgCard);
    }, 500);


    scores[0].innerText = `Score: ${pointPlayer}`;

    if (pointPlayer > 21) {
        console.warn('Lose, did Win Computer');
        btnNew.disabled = false;
        btnStop.disabled = true;
        btnTakeCard.disabled = true;
        setTimeout(() => {
            computerShitf(pointPlayer);
        }, 1000);
    } else if (pointPlayer === 21) {
        console.warn('Nobody did win');
        btnNew.disabled = false;
        btnStop.disabled = true;
        btnTakeCard.disabled = true;
        setTimeout(() => {
            computerShitf(pointPlayer);
        }, 1000);

    }


});

btnStop.addEventListener('click', () => {
    computerShitf(pointPlayer);

    btnStop.disabled = true;
    btnTakeCard.disabled = true;
    btnNew.disabled = false;
});

btnNew.addEventListener('click', () => {
    deck = [];
    deckBack = [];
    divCardBack.innerHTML = '';
    createDeck();
    createBackCard(deck);

    btnNew.disabled = true;
    btnTakeCard.disabled = false;
    btnStop.disabled = false;
    pointPlayer = 0;
    pointComputer = 0;

    divCardComputer.innerHTML = '';
    divCardPlayer.innerHTML = '';

    scores[0].innerText = 'Score: 0';
    scores[1].innerText = 'Score: 0';

});