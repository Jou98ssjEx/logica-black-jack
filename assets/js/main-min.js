const myModule = (() => { "use strict"; let e = [],
        t = [],
        r = [],
        d = ""; const l = ["C", "D", "H", "S"],
        n = ["A", "J", "Q", "K"],
        s = document.querySelector("#btnNew"),
        a = document.querySelector("#btnTakeCard"),
        c = document.querySelector("#btnStop"),
        o = document.querySelectorAll("#divCardPlayer"),
        i = document.querySelectorAll("#divCardBack"),
        u = document.querySelectorAll("span"),
        m = document.querySelectorAll("small");
    a.disabled = !0, c.disabled = !0; const p = (d = 2) => { r = [], i.forEach(e => e.innerHTML = ""), e = h(), t = []; for (let e = 0; e < d; e++) t.push(0);
            m.forEach(e => e.innerText = "Score: 0"), o.forEach(e => e.innerHTML = ""), a.disabled = !1, c.disabled = !1, b() },
        b = () => { u[3].innerText = "", d = prompt("Insert your name: "), u[3].innerText = d },
        h = () => { e = []; for (let t = 2; t <= 10; t++)
                for (let r of l) e.push(t + r); for (let t of n)
                for (let r of l) e.push(t + r); return f(e), _.shuffle(e) },
        f = e => { for (let t = 1; t <= e.length; t++) { let e = document.createElement("img");
                e.classList.add("cardBack"), e.id = `card${t}`, e.src = "assets/cards/grey_back.png", r.push(e), divCardBack.append(e) } },
        g = () => (0 === e.length && console.log("Don´t card in the deck"), e.pop()),
        y = (e, r) => (t[r] = t[r] + (e => { let t = e.substring(0, e.length - 1); return t = isNaN(t) ? "A" === t ? 11 : 10 : 1 * t })(e), m[r].innerText = `Score: ${t[r]}`, t[r]),
        S = (e, t) => { const d = document.createElement("img");
            d.src = `assets/cards/${e}.png`, d.classList.add("img"), o[t].append(d), setTimeout(() => { d.classList.remove("img"), d.classList.add("cardM") }, 10); let l = r.pop(); return l.classList.add("cardMov"), setTimeout(() => { l.remove() }, 400), d },
        T = e => { let r = 0;
            do { let e = g();
                r = y(e, t.length - 1), S(e, t.length - 1) } while (e > r && e <= 21);
            (() => { let [e, r] = t;
                setTimeout(() => { r === e ? alert("Nobody Win") : e > 21 ? alert("Did win computer") : r > 21 ? alert(`You win ${d}`) : alert("Did win computer") }, 800) })() }; return btnTakeCard.addEventListener("click", () => { let e = g(); const t = y(e, 0);
        S(e, 0), t > 21 ? (a.disabled = !0, c.disabled = !0, T(t)) : 21 === t && (a.disabled = !0, c.disabled = !0, T(t)) }), c.addEventListener("click", () => { a.disabled = !0, c.disabled = !0, T(t[0]) }), s.addEventListener("click", () => { p() }), { newGame: p } })();