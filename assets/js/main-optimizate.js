const myModule = (() => {
    'use strict'
    /*===== Global variable =====*/
    let deck = [],
        pointerPlayers = [], // Player: First, Computer lastNumber.
        deckBack = [],
        namePlayer = '';

    const types = ['C', 'D', 'H', 'S'],
        specials = ['A', 'J', 'Q', 'K'];

    /*===== Buttons variable =====*/
    const btnNew = document.querySelector('#btnNew'),
        btnTake = document.querySelector('#btnTakeCard'),
        btnStop = document.querySelector('#btnStop');

    /*===== HTML variable =====*/
    const divCardPlayers = document.querySelectorAll('#divCardPlayer'),
        CardBack = document.querySelectorAll('#divCardBack'),
        nameP = document.querySelectorAll('span'),
        scores = document.querySelectorAll('small');

    btnTake.disabled = true;
    btnStop.disabled = true;

    // ================ Init Game ========================
    const InitGAme = (numPlayers = 2) => {
        deckBack = [];
        CardBack.forEach(elem => elem.innerHTML = '');

        deck = createDeck();
        pointerPlayers = [];
        for (let i = 0; i < numPlayers; i++) {
            pointerPlayers.push(0);
        }

        scores.forEach(elem => elem.innerText = 'Score: 0');
        divCardPlayers.forEach(elem => elem.innerHTML = '');

        btnTake.disabled = false;
        btnStop.disabled = false;
        dataPlayer();
    }

    // ================ Init Game ========================
    const dataPlayer = () => {
        nameP[3].innerText = '';

        namePlayer = prompt('Insert your name: ');

        nameP[3].innerText = namePlayer;
    }

    // ================ Create Deck ========================
    const createDeck = () => {
        deck = [];
        /* Insert the number more types in the deck */
        for (let i = 2; i <= 10; i++) {
            // console.log(i);
            for (let type of types) {
                deck.push(i + type)
            }
        }

        /* Insert the number more specials in the deck */
        for (let spe of specials) {
            // console.log(spe);
            for (let type of types) {
                deck.push(spe + type);
            }
        }

        /* .-shuffle: returns the randomly shuffed deck of cards    */
        createBackCard(deck);
        return _.shuffle(deck);
    }

    /*==================== CREATE ELEMENT IMG BACK CARD ====================*/
    const createBackCard = (deck) => {

        for (let i = 1; i <= deck.length; i++) {

            let imgCard = document.createElement('img');
            imgCard.classList.add('cardBack');
            imgCard.id = `card${i}`
            imgCard.src = `assets/cards/grey_back.png`;

            deckBack.push(imgCard);
            divCardBack.append(imgCard);
        }
    }

    // ================ Take Card ========================
    const takeCard = () => {

        if (deck.length === 0) {
            console.log('Don´t card in the deck');
        }
        return deck.pop();
    }

    // ================ Value Card Min ========================
    const valueCard = (card) => {
        let value = card.substring(0, card.length - 1);
        value = (isNaN(value)) ? (value === 'A') ? 11 : 10 : value * 1;
        return value;
    }

    // ================ Acumulate Pointers of Player ========================
    const collectPoints = (card, shift) => {
        pointerPlayers[shift] = pointerPlayers[shift] + valueCard(card);
        scores[shift].innerText = `Score: ${pointerPlayers[shift]}`;
        return pointerPlayers[shift];
    }

    // ================ Create Cards ========================
    const createCards = (card, shift) => {
        const imgCard = document.createElement('img');
        imgCard.src = `assets/cards/${card}.png`;
        imgCard.classList.add('img');
        divCardPlayers[shift].append(imgCard);
        // divComputerCard.append(imgCard);

        setTimeout(() => {
            imgCard.classList.remove('img');
            imgCard.classList.add('cardM');
        }, 10);

        let cardB = deckBack.pop();
        cardB.classList.add('cardMov');

        setTimeout(() => {
            cardB.remove();
        }, 400);

        return imgCard;
    }

    // ================ Determine Winner ========================
    const determineWinner = () => {

        let [pointMin, pointComputer] = pointerPlayers; //Desctructure Object
        setTimeout(() => {
            if (pointComputer === pointMin) {
                alert('Nobody Win');
            } else if (pointMin > 21) {
                alert('Did win computer');

            } else if (pointComputer > 21) {
                alert(`You win ${namePlayer}`);

            } else {
                alert('Did win computer');

            }
        }, 800);

    }

    // ================ Computer Shift ========================
    const computerShitf = (pointMin) => {
        let pointComputer = 0;
        do {
            let card = takeCard();
            pointComputer = collectPoints(card, pointerPlayers.length - 1);
            createCards(card, pointerPlayers.length - 1);

        } while ((pointMin > pointComputer) && (pointMin <= 21))

        determineWinner();

    }

    // ================ Envents ========================
    btnTakeCard.addEventListener('click', () => {
        let card = takeCard();

        const pointPlayer = collectPoints(card, 0);
        createCards(card, 0);

        if (pointPlayer > 21) {
            // console.warn('Sorry, Lose!');
            btnTake.disabled = true;
            btnStop.disabled = true;

            computerShitf(pointPlayer);

        } else if (pointPlayer === 21) {
            btnTake.disabled = true;
            btnStop.disabled = true;

            // console.warn('Good');
            computerShitf(pointPlayer);

        }
    });

    btnStop.addEventListener('click', () => {
        btnTake.disabled = true;
        btnStop.disabled = true;
        computerShitf(pointerPlayers[0]);
    });

    btnNew.addEventListener('click', () => {
        InitGAme();
    });

    return {
        newGame: InitGAme
    }
})();